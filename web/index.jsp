<%--
  Created by IntelliJ IDEA.
  User: sch9910see
  Date: 2021/10/3
  Time: 16:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
    $(function(){
        $("#uname").blur(function(){
            //1.获得value值
            var uname = $(this).val();
            //2.发送请求
            /* $.ajax({
               url:"/check",
               data:"username="+uname,
               type:"post",
               dataType:"text",
               success:function(username_msg){
                 $("#username_msg").html(username_msg);
               }
                });
               */
            /* $.get("/check","username="+uname,function(username_msg){
                 $("#username_msg").html(username_msg);
              });*/

            $.post("/check","username="+uname,function(username_msg){

                    $("#username_msg").html(username_msg).css("color", "grey");

            });
        });

    })

</script>
<body>

<form method="post" action="#">
    <table>
        <tr>
            <td>用户名:</td>
            <td><input type="text"  id="uname" ></td>
            <td><span id="username_msg"></span></td>
        </tr>
        <tr>
            <td>密码:</td>
            <td><input type="text" name="password"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan='3'><input type="submit" id="sub"></td>
        </tr>
    </table>
</form>
</body>
</html>
