package Servlet;

import bean.Users;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/get")
public class SearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 获取参数.
        String uid = req.getParameter("uid");
        int userid = Integer.parseInt(uid);
        Users users = null;

        switch (userid) {
            case 1:
                users=new Users(1,"小明","758200",18);
                break;
            case 2:
                users=new Users(2,"小芳","666600",22);
                break;
            case 3:
                users=new Users(3,"小强","123400",14);
                break;
            default:
                users = new Users();

        }
        // 将查询到的数据 , 转换为json格式.即java-->json
        JSONObject jsonObject  = JSONObject.fromObject(users);
        //编码格式
        resp.setContentType("text/html; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.print(jsonObject);


    }
}
